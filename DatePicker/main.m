//
//  main.m
//  DatePicker
//
//  Created by Ali Farhan on 07/04/2015.
//  Copyright (c) 2015 Ali Farhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  ViewController.m
//  DatePicker
//
//  Created by Ali Farhan on 07/04/2015.
//  Copyright (c) 2015 Ali Farhan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <AADatePickerDelegate>

@property (strong, nonatomic) UILabel *dateLabel;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.datepicker_native setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    self.datepicker_native.backgroundColor = [UIColor colorWithRed:0.0/255.0f green:153.0/255.0f blue:233.0/255.0f alpha:1];
    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
                                [UIDatePicker
                                 instanceMethodSignatureForSelector:selector]];
    BOOL no = YES;    //if yes then will show today for the date else vice versa
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:self.datepicker_native];
    
    
    
    
    
    
    
    
    
    
    [self show_date_picker];
    

}
-(void) show_date_picker
{
    //set max date for number of years you want to activate the picker to go.   10 years=10*365*24*60*60
    AADatePicker *datePicker = [[AADatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height/2, self.view.bounds.size.width, 200) maxDate:[NSDate dateWithTimeIntervalSinceNow:10*365*24*60*60] minDate:[NSDate date] showValidDatesOnly:NO picker_background_color:[UIColor colorWithRed:0.0/255.0f green:153.0/255.0f blue:233.0/255.0f alpha:1] font_size:25.0 is_Text_Bold:NO];
    
    datePicker.delegate = self;
    [self.view addSubview:datePicker];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)dateChanged:(AADatePicker *)sender
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:sender.date
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterMediumStyle];
    NSLog(@"%@",dateString);
    
    //[self.dateLabel setText:dateString];
}





- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = @"sample title";
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
@end
